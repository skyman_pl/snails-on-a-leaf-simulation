#include <QApplication>
#include <QTranslator>
#include <QString>
#include "ui/mainwindow.h"
#include "ui/drawleafadapter.h"
#include "sim/snailsonleafsimulation.h"
#include "middle/simguiconnector.h"

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  QTranslator translator;

  const char* TRANSLATION_FILE_PREFIX = "snails_";
  const char* TRANSLATION_PATH = ":/translations";
  translator.load(QString(TRANSLATION_FILE_PREFIX) + QLocale::system().name(),TRANSLATION_PATH);
  a.installTranslator(&translator);

  MainWindow w;

  Sim::SnailsOnLeafSimulation sim(w.DrawingAdapterFactory());

  Middle::SimGuiConnector guiSimConn(w, sim);

  w.show();

  return a.exec();
}
