<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">de mainwindow</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>Snails on a leaf simulation</source>
        <translation>Simulation von Schnecken auf einem Blatt</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="57"/>
        <source>Number of rows</source>
        <translation>Zeilenmenge</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="89"/>
        <source>Number of columns</source>
        <translation>Spaltenmenge</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="121"/>
        <source>Number of snails</source>
        <translation>Schneckenmenge</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="50"/>
        <source>Stop simulation</source>
        <translation>Simulation beenden</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="52"/>
        <source>Start simulation</source>
        <translation>Simulation starten</translation>
    </message>
</context>
</TS>
