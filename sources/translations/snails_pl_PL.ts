<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">pl mainwindow</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>Snails on a leaf simulation</source>
        <translation>Symulacja ślimaków na liściu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="57"/>
        <source>Number of rows</source>
        <translation>Liczba wierszy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="89"/>
        <source>Number of columns</source>
        <translation>Liczba kolumn</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="121"/>
        <source>Number of snails</source>
        <translation>Liczba ślimaków</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="50"/>
        <source>Stop simulation</source>
        <translation>Zatrzymaj symulację</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="52"/>
        <source>Start simulation</source>
        <translation>Uruchom symulację</translation>
    </message>
</context>
</TS>
