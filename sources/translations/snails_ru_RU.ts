<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">ru mainwindow</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>Snails on a leaf simulation</source>
        <translatorcomment>симуляция улиток на листе</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="57"/>
        <source>Number of rows</source>
        <translation>количество рядов</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="89"/>
        <source>Number of columns</source>
        <translation>количество столбцов</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="121"/>
        <source>Number of snails</source>
        <translation>количество улиток</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="50"/>
        <source>Stop simulation</source>
        <translation>остановить симуляцию</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="52"/>
        <source>Start simulation</source>
        <translation>начать симуляцию</translation>
    </message>
</context>
</TS>
