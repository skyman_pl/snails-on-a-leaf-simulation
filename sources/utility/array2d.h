#ifndef MATRIX2D_H
#define MATRIX2D_H

#include <vector>

namespace Utility
{
  template <typename T>
  class Array2d;
}

template <typename T>
class Utility::Array2d
{
public:
  class Row_t
  {
  public:
    Row_t(const size_t& intendedCols);

    void SetIntendedSize(const size_t& intendetCols);
    void AddCellBack(const T& newCell);
    void AddCellBack(T&& newCell);
    T& At(const size_t col);
    const T& At(const size_t col) const;
    size_t getColsCount() const;
    size_t getRowCapacity() const;
    void Clear();

  private:
    std::vector<T> _colsArr;
  };

  Array2d();
  Array2d(const size_t& intendedRows, const size_t& intendedCols);

  void SetIntendedSize(const size_t& intendedRows, const size_t& intendetCols);
  Row_t& prepareRowBack();
  T& At(const size_t row, const size_t col);
  const T& At(const size_t row, const size_t col) const;
  size_t getRowsCount() const;
  size_t getColsCount() const;
  size_t getIntendedColsCount() const;
  size_t getIntendedRowsCount() const;
  void Clear();

private:
  size_t _colsIntendedCount;
  std::vector<Row_t> _rowsArr;
};


/* Fix for: undefined reference to `Utility::Array2d<QGraphicsRectItem*>::Array2d(unsigned long long const&, unsigned long long const&)' */

/*!
 * \class Utility::Array2d<T>
 * \brief Small wrapper template for 2D std::vector array.
 *
 * \relates std::vector
 * \since v0.0
 * This class wraps std::vector<std::vector<T>> for easier usage, allows to create a 2D Matrix row by row,
 * starting from the top. Internally it contains a std::vector<Row_t>, which in turn is a wrapper for std::vector<T>.
 * This allows to grab the reference to the row being filled and then add new elements. This class is minimalistic in a sense,
 * that assumes, that all rows have equal length and has almost none additional functionalities needed in this simple project.
 */

/*!
 * \fn Utility::Array2d<T>::Array2d()
 * \brief Default constructor, reserves no space for future elements.
 */
template <typename T>
Utility::Array2d<T>::Array2d()
  : Array2d(0,0)
{

}

/*!
 * \brief Prefered constructor, reserves memory for new elements.
 * \param intendedRows Number of rows to be reserved.
 * \param intendedCols Number of columns to be reserved.
 *
 * Internally no space for columns is being reserved, since no rows exist.
 * Thus the value of intendedCols is simply stored in a private field to be used on row's creation.
 */
template <typename T>
Utility::Array2d<T>::Array2d(const size_t& intendedRows, const size_t& intendetCols)
{
  _colsIntendedCount = intendetCols;
  _rowsArr.reserve(intendedRows);
}

/*!
 * \fn Utility::Array2d<T>::Row_t& Utility::Array2d<T>::prepareRowBack()
 * \brief Creates new row at the back (end) and then returns it.
 * \return reference to newly created row.
 *
 * Wraps std::vector<T>::emplace_back().
 */
template <typename T>
typename Utility::Array2d<T>::Row_t& Utility::Array2d<T>::prepareRowBack()
{
  _rowsArr.emplace_back(Row_t(_colsIntendedCount));

  return _rowsArr.back();
}

/*!
 * \fn T& Utility::Array2d<T>::At(const size_t row, const size_t col)
 * \brief Allows access to particular element in the array usind row and column index.
 * \param row Row index.
 * \col Column index.
 *
 * Forwards call to std::vector<T>::at()
 */
template <typename T>
T& Utility::Array2d<T>::At(const size_t row, const size_t col)
{
  return _rowsArr.at(row).At(col);
}

/*!
 * \fn const T& Utility::Array2d<T>::At(const size_t row, const size_t col) const
 * \overload Utility::Array2d<T>::At(const size_t row, const size_t col)
 */
template <typename T>
const T& Utility::Array2d<T>::At(const size_t row, const size_t col) const
{
  return _rowsArr.at(row).At(col);
}

/*!
 * \fn void Utility::Array2d<T>::Clear()
 * \brief Deletes all elements in the array, without explicitly calling their destructors.
 *
 * Calls std::vector<T>::clear() for each row and then for entire array, then resets reserved memory to 0.
 */
template <typename T>
void Utility::Array2d<T>::Clear()
{
  for(auto& it : _rowsArr)
  {
    it.Clear();
  }

  _colsIntendedCount = 0;
  _rowsArr.clear();
  _rowsArr.shrink_to_fit();
}

/*!
 * \fn void Utility::Array2d<T>::SetIntendedSize(const size_t& intendedRows, const size_t& intendetCols)
 * \brief Reserves space for new elements.
 * \param intendedRows the space will be reserved for this number of rows
 * \param intendedCols the space will be reserved for this number of columns
 *
 * Calls internally std::vector<T>::reserve() for row array, but stores column count in a private field, since no row exist.
 * The intended column count is going to be used when new row will be created.
 */
template <typename T>
void Utility::Array2d<T>::SetIntendedSize(const size_t& intendedRows, const size_t& intendedCols)
{
  _colsIntendedCount = intendedCols;
  _rowsArr.reserve(intendedRows);
}

/*!
 * \fn size_t Utility::Array2d<T>::getRowsCount() const
 * \brief Returns number of created rows.
 * \return Number of created rows.
 */
template <typename T>
size_t Utility::Array2d<T>::getRowsCount() const
{
  return _rowsArr.size();
}

/*!
 * \fn size_t Utility::Array2d<T>::getColsCount() const
 * \brief Returns number of created columns in the first row.
 * \return Number of created columns in the first row.
 */
template <typename T>
size_t Utility::Array2d<T>::getColsCount() const
{
  return _rowsArr.empty() ? 0 : _rowsArr.cbegin()->getColsCount();
}

/*!
 * \fn size_t getIntendedColsCount() const
 * The method internally calls vector<T>::capacity()
 * \returns Returns capacity for columns in the first row
 */
template <typename T>
size_t Utility::Array2d<T>::getIntendedColsCount() const
{
  return _rowsArr.empty() ? _colsIntendedCount : _rowsArr.cbegin()->getRowCapacity();
}

/*!
 * \fn size_t getIntendedRowsCount() const
 * The method internally calls vector<T>::capacity()
 * \returns Returns capacity for columns in the first row
 */
template <typename T>
size_t Utility::Array2d<T>::getIntendedRowsCount() const
{
  return _rowsArr.capacity();
}

/*!
 * \class Utility::Array2d<T>::Row_t
 * \brief The Row_t class is a simple wrapper for std::vector<T>. It supports operation on a single row of a Array2d template.
 *
 * \since v0.0
 */

/*!
 * \fn Utility::Array2d<T>::Row_t::Row_t(const size_t& intendedCols)
 * \brief constructor of Row_t
 * \param intendedCols
 *
 * forwards the number of columns to std::vector<T>::reserve(). No items are created.
 */
template <typename T>
Utility::Array2d<T>::Row_t::Row_t(const size_t& intendedCols)
{
  _colsArr.reserve(intendedCols);
}

/*!
 * \fn void Utility::Array2d<T>::Row_t::SetIntendedSize(const size_t& intendetCols)
 * \brief Once more it is a wrapper to std::vector<T>::reserve().
 * \param intendedCols the space will be reserved for this number of columns
 *
 * Calls internally std::vector<T>::reserve(), so the space can only grow.
 */
template <typename T>
void Utility::Array2d<T>::Row_t::SetIntendedSize(const size_t& intendedCols)
{
  _colsArr.reserve(intendedCols);
}

/*!
 * \fn void Utility::Array2d<T>::Row_t::AddCellBack(const T& newCell)
 * \brief Adds new element to internal std::vector<T> at the back (end).
 * \param newCell reference to a new item to be added to the row
 *
 * Invokes internally std::vector<T>::push_back().
 */
template <typename T>
void Utility::Array2d<T>::Row_t::AddCellBack(const T& newCell)
{
  _colsArr.push_back(newCell);
}

/*!
 * \fn void Utility::Array2d<T>::Row_t::AddCellBack(T&& newCell)
 * \overload Utility::Array2d<T>::Row_t::AddCellBack(const T&)
 */
template <typename T>
void Utility::Array2d<T>::Row_t::AddCellBack(T&& newCell)
{
  _colsArr.push_back(newCell);
}

/*!
 * \fn T& Utility::Array2d<T>::Row_t::At(const size_t col)
 * \brief Wrapper for std::vector<T>::at()
 * \param col column index
 * \return reference to the stored element
 */
template <typename T>
T& Utility::Array2d<T>::Row_t::At(const size_t col)
{
  return _colsArr.at(col);
}

/*!
 * \fn const T& Utility::Array2d<T>::Row_t::At(const size_t col) const
 * \overload Utility::Array2d<T>::Row_t::AddCellBack(const T&)
 */
template <typename T>
const T& Utility::Array2d<T>::Row_t::At(const size_t col) const
{
  return _colsArr.at(col);
}

/*!
 * \fn size_t Utility::Array2d<T>::Row_t::getColsCount() const
 * \brief Returns the result of std::vector<T>::size()
 * \return rvalue reference to the items count
 */
template <typename T>
size_t Utility::Array2d<T>::Row_t::getColsCount() const
{
  return _colsArr.size();
}

/*!
 * \fn size_t Utility::Array2d<T>::Row_t::getRowCapacity() const
 * \brief Returns the result of std::vector<T>::capacity()
 * \return rvalue reference to set capacity of underlying container
 */
template <typename T>
size_t Utility::Array2d<T>::Row_t::getRowCapacity() const
{
  return _colsArr.capacity();
}


/*!
 * \fn void Utility::Array2d<T>::Row_t::Clear()
 * \brief Deletes all elements of the internal std::vector<T>
 */
template <typename T>
void Utility::Array2d<T>::Row_t::Clear()
{
  _colsArr.clear();
  _colsArr.shrink_to_fit();
}

#endif // MATRIX2D_H
