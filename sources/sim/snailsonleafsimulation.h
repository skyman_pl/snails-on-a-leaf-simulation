#ifndef SNAILSONLEAFSIMULATION_H
#define SNAILSONLEAFSIMULATION_H

#include "../ui/drawleafadapter.h"
#include "../middle/simparametersstruct.h"
#include <memory>

namespace Sim
{
  class SnailsOnLeafSimulation;
}

class Sim::SnailsOnLeafSimulation : public QObject
{
  Q_OBJECT

public:
  SnailsOnLeafSimulation(std::shared_ptr<Ui::AbstractDrawingAdapter>& drawingAdapter);

public slots:
  void startSimulation(SimParametersStruct* simParams);
  void stopSimulation();

private:
  std::shared_ptr<Ui::AbstractDrawingAdapter> _drawingAdapter;
};

#endif // SNAILSONLEAFSIMULATION_H
