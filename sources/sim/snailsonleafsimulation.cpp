/*!
 * \class Sim::SnailsOnLeafSimulation
 * \brief WIP
 * \inheaderfile snailsonleafsimulation.h
 * \since v0.0
 *
 * Simulation component (business logic), performs the simulation according to signals from GUI
 * and visualizes the results on a canvas using Ui::AbstractDrawingAdapter .
 */

#include "snailsonleafsimulation.h"
#include <QObject>

Sim::SnailsOnLeafSimulation::SnailsOnLeafSimulation(std::shared_ptr<Ui::AbstractDrawingAdapter>& drawingAdapter)
{
  _drawingAdapter = drawingAdapter;
}

void Sim::SnailsOnLeafSimulation::startSimulation(SimParametersStruct* simParams)
{
  _drawingAdapter->DrawTheLeaf(simParams->NumberOfRows, simParams->NumberOfCols);
}

void Sim::SnailsOnLeafSimulation::stopSimulation()
{
  _drawingAdapter->ClearTheCanvas();
}
