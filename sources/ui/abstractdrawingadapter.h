#ifndef ABSTRACTDRAWINGADAPTER_H
#define ABSTRACTDRAWINGADAPTER_H

#include <cstdint>

namespace Ui
{
  class AbstractDrawingAdapter;
}

class Ui::AbstractDrawingAdapter
{
public:
  AbstractDrawingAdapter();
  virtual ~AbstractDrawingAdapter();
  virtual void DrawTheLeaf(const uint8_t& rowsCount, const uint8_t& colsCount);
  virtual void ClearTheCanvas();

};

#endif // ABSTRACTDRAWINGADAPTER_H
