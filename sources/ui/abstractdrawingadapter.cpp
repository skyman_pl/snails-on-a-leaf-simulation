/*!
 * \class Ui::AbstractDrawingAdapter
 * \brief WIP
 * \inheaderfile abstractdrawingadapter.h
 * \since v0.0
 * \interface Ui::GraphicsViewDrawAdapter
 *
 * Serves as a abstraction layer between GUI and Simulation and receives high level commands
 * from Simulation component.
 */

#include "abstractdrawingadapter.h"

Ui::AbstractDrawingAdapter::AbstractDrawingAdapter()
{

}

Ui::AbstractDrawingAdapter::~AbstractDrawingAdapter()
{

}

void Ui::AbstractDrawingAdapter::DrawTheLeaf(const uint8_t&, const uint8_t&)
{

}

void Ui::AbstractDrawingAdapter::ClearTheCanvas()
{

}
