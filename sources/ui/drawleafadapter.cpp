/*!
 * \class Ui::GraphicsViewDrawAdapter
 * \brief WIP
 * \inheaderfile drawleafadapter.h
 * \implements Ui::AbstractDrawingAdapter
 * \since v0.0
 *
 * Receives high level commands to draw on the canvas of the GUI, to which has direct control.
 * Stores raw pointer ( \relates Qt::QGraphicsscene::addRect ) to each cell drawn.
 */

#include "drawleafadapter.h"
#include <QRect>
#include <QGraphicsRectItem>

Ui::GraphicsViewDrawAdapter::GraphicsViewDrawAdapter(QMainWindow* parentWindow, QGraphicsView &canvas)
  : Ui::AbstractDrawingAdapter(), _scene(parentWindow), _canvas(canvas)
{
  _canvas.setScene(&_scene);
  _canvas.setFrameStyle(1);

  _borderPen = QPen(Qt::PenStyle::SolidLine);
  _borderPen.setWidth(1);
  _borderPen.setColor(QColor(0,0,0));
  _emptyBrush = QBrush(QColor(255,255,255));
}

Ui::GraphicsViewDrawAdapter::~GraphicsViewDrawAdapter()
{

}

void Ui::GraphicsViewDrawAdapter::DrawTheLeaf(const uint8_t& rowsCount, const uint8_t& colsCount)
{
  auto canvasFrameWidth = _canvas.frameStyle();
  auto canvasWidth = _canvas.width();
  auto canvasHeight = _canvas.height();

  // Fit scene to visible QGraphicsView without scrollbars
  _canvas.setSceneRect(0,0,canvasWidth-2*canvasFrameWidth,canvasHeight-2*canvasFrameWidth);

  auto rectHeight = (canvasHeight - canvasFrameWidth*2)/rowsCount;
  auto rectWidth = (canvasWidth - canvasFrameWidth*2)/colsCount;

  _rectArr.SetIntendedSize(static_cast<size_t>(rowsCount), static_cast<size_t>(colsCount));
  for (auto r = 0u; r < rowsCount; ++r)
  {
      auto& newRow = _rectArr.prepareRowBack();

      for (auto c = 0u; c < colsCount; ++c)
      {
          auto rect = QRect(rectWidth*static_cast<int>(c),rectHeight*static_cast<int>(r),rectWidth,rectHeight);
          auto ptrItem =_scene.addRect(rect, _borderPen, _emptyBrush);
          newRow.AddCellBack(ptrItem);
      }
  }
}

void Ui::GraphicsViewDrawAdapter::ClearTheCanvas()
{
  _rectArr.Clear();
  _scene.clear();
}
