#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>

#include "drawleafadapter.h"
#include "../middle/simparametersstruct.h"

namespace Ui
{
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow() override;

  std::shared_ptr<Ui::AbstractDrawingAdapter>& DrawingAdapterFactory();

signals:
  void startSimulation(SimParametersStruct* simParams);
  void stopSimulation();

protected:
  void changeEvent(QEvent* event) override;

private:
  void onStartStopButtonClick();
  void startStopButtonTextUpdate();
  SimParametersStruct* collectParameters() const;

  Ui::MainWindow *ui;
  std::shared_ptr<Ui::AbstractDrawingAdapter> _drawLeafAdapter;
  bool _isSimRunning;
};

#endif // MAINWINDOW_H
