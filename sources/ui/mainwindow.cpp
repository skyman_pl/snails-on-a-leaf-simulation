/*!
 * \class MainWindow
 * \brief WIP
 * \inheaderfile mainwindow.h
 * \since v0.0
 *
 * Implements presentation layer of the application. Redirects user actions to simulation component.
 * Shares with simulation module a separate component used to directly draw on the canvas,
 * serving as abstraction layer between GUI and business logic (simulation).
 */

#include <cstdint>
#include <QObject>
#include <QPushButton>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  _isSimRunning(false)
{
  ui->setupUi(this);
  startStopButtonTextUpdate();

  connect(ui->pushButtonStartStop, &QPushButton::clicked, this, &MainWindow::onStartStopButtonClick);
}

std::shared_ptr<Ui::AbstractDrawingAdapter>& MainWindow::DrawingAdapterFactory()
{
  if(!_drawLeafAdapter)
    _drawLeafAdapter = std::make_shared<Ui::GraphicsViewDrawAdapter>(this, *ui->graphicsView);

  return _drawLeafAdapter;
}

void MainWindow::onStartStopButtonClick()
{
  if(_isSimRunning)
    emit stopSimulation();
  else
    emit startSimulation(collectParameters());

  _isSimRunning = !_isSimRunning;
  startStopButtonTextUpdate();
}

void MainWindow::changeEvent(QEvent* event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        // retranslate designer form (single inheritance approach)
        ui->retranslateUi(this);
    }

    // remember to call base class implementation
    QMainWindow::changeEvent(event);
}

void MainWindow::startStopButtonTextUpdate()
{
  if(_isSimRunning)
    ui->pushButtonStartStop->setText(tr("Stop simulation"));
  else
    ui->pushButtonStartStop->setText(tr("Start simulation"));
}

SimParametersStruct* MainWindow::collectParameters() const
{
  auto params = new SimParametersStruct{};
  params->NumberOfRows = static_cast<uint8_t>(ui->spinBoxRowsCount->value());
  params->NumberOfCols = static_cast<uint8_t>(ui->spinBoxColsCount->value());
  params->NumberOfSnails = static_cast<uint8_t>(ui->spinBoxSnailsCount->value());

  return params;
}

MainWindow::~MainWindow()
{
  delete ui;
}
