#ifndef DRAWLEAFADAPTER_H
#define DRAWLEAFADAPTER_H

#include <QMainWindow>
#include <QGraphicsView>
#include "abstractdrawingadapter.h"
#include "../utility/array2d.h"

namespace Ui
{
  class GraphicsViewDrawAdapter;
}

class Ui::GraphicsViewDrawAdapter : public Ui::AbstractDrawingAdapter
{
public:
  GraphicsViewDrawAdapter(QMainWindow* parentWindow, QGraphicsView &canvas);
  ~GraphicsViewDrawAdapter() override;
  void DrawTheLeaf(const uint8_t& rowsCount, const uint8_t& colsCount) override;
  void ClearTheCanvas() override;

private:
  QGraphicsScene _scene;
  QGraphicsView& _canvas;
  QPen _borderPen;
  QBrush _emptyBrush;
  Utility::Array2d<QGraphicsRectItem*> _rectArr;
};

#endif // DRAWLEAFADAPTER_H
