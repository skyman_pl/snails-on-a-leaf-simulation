#ifndef SIMGUICONNECTOR_H
#define SIMGUICONNECTOR_H

#include "../ui/mainwindow.h"
#include "../sim/snailsonleafsimulation.h"

namespace Middle
{
  class SimGuiConnector;
}

class Middle::SimGuiConnector
{
public:
  SimGuiConnector(MainWindow& guiObject, Sim::SnailsOnLeafSimulation& simObject);
};

#endif // SIMGUICONNECTOR_H
