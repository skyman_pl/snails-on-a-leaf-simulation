#ifndef SIMPARAMETERSSTRUCT_H
#define SIMPARAMETERSSTRUCT_H

#include <cstdint>

struct SimParametersStruct
{
  uint8_t NumberOfRows;
  uint8_t NumberOfCols;
  uint8_t NumberOfSnails;
};

#endif // SIMPARAMETERSSTRUCT_H
