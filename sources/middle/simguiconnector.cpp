/*!
 * \class Middle::SimGuiConnector
 * \brief WIP
 * \inheaderfile simguiconnector.h
 * \since v0.0
 *
 * Binds slots and signals of GUI and Simulation component to couple them.
 */

#include "simguiconnector.h"
#include <QObject>

Middle::SimGuiConnector::SimGuiConnector(MainWindow& guiObject, Sim::SnailsOnLeafSimulation& simObject)
{
  QObject::connect(&guiObject, &MainWindow::startSimulation, &simObject, &Sim::SnailsOnLeafSimulation::startSimulation);
  QObject::connect(&guiObject, &MainWindow::stopSimulation, &simObject, &Sim::SnailsOnLeafSimulation::stopSimulation);
}
