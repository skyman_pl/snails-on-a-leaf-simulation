UNITTEST_DIR = unittests
BUSINESS_DIR = sources

include(gtest_dependency.pri)
include(business_logic_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

HEADERS += \
        $${UNITTEST_DIR}/tst_array2dRowtFunctionality.h \
        $${UNITTEST_DIR}/tst_array2dAtomicOperations.h \
        $${UNITTEST_DIR}/tst_array2dUsageScenarios.h \
        $${UNITTEST_DIR}/row_tallpublic.h

SOURCES += \
        $${UNITTEST_DIR}/main.cpp \

RESOURCES +=


