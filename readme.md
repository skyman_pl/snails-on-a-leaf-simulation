Snails on a leaf simulation.

This application is meant to be a QT demo and is referenced in my Curriculum Vitae. I may here employ different design patterns, just to learn about them, which would not necessarily be an obvious choice in this simple example, but would benefit me regardless.

The premise is to simulate a small snail farm located on an arbitrarily big rectangular leaf. The leaf consists of a number of sectors, in which the snails could live. One sector can support only one snail and it is up to the snail to find a sweet spot for itself. The leaf also regenerates itself automatically, so the snails have something to eat.

In basic version the controls are pretty self explanatory, you can select how big your leaf would be in a sense of sector count and set number of snails to simulate. The leaf woudl take as much as visible space as possible without triggering the scrollbars to show. You can tell which sectors are healthy, partially eaten or dead by their colour. Snails would also be shown somehow. Upon the start the leaf is going to be drawn and threads for the snails are going to be dispatched. Once the simulation ist stopped all dynanamic data is going to be wiped out, including the leaf and the threads are going to be terminated. The simulation can run forever, the only measure to stop it is to press the stop button.

This software is licensed under GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007.
