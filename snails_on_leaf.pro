#-------------------------------------------------
#
# Project created by QtCreator 2019-03-06T15:32:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = snails_on_leaf
TEMPLATE = app
SOURCES_DIR = sources

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14
CONFIG += Wall

SOURCES += \
    $${SOURCES_DIR}/main.cpp \
    $${SOURCES_DIR}/ui/mainwindow.cpp \
    $${SOURCES_DIR}/ui/drawleafadapter.cpp \
    $${SOURCES_DIR}/sim/snailsonleafsimulation.cpp \
    $${SOURCES_DIR}/ui/abstractdrawingadapter.cpp \
    $${SOURCES_DIR}/middle/simguiconnector.cpp

HEADERS += \
    $${SOURCES_DIR}/ui/mainwindow.h \
    $${SOURCES_DIR}/ui/drawleafadapter.h \
    $${SOURCES_DIR}/sim/snailsonleafsimulation.h \
    $${SOURCES_DIR}/ui/abstractdrawingadapter.h \
    $${SOURCES_DIR}/middle/simguiconnector.h \
    $${SOURCES_DIR}/middle/simparametersstruct.h \
    $${SOURCES_DIR}/sim/leafstructure.h \
    $${SOURCES_DIR}/utility/array2d.h

FORMS += \
    $${SOURCES_DIR}/ui/mainwindow.ui

TRANSLATIONS += \
    $${SOURCES_DIR}/translations/snails_de_DE.ts \
    $${SOURCES_DIR}/translations/snails_pl_PL.ts \
    $${SOURCES_DIR}/translations/snails_ru_RU.ts

DISTFILES += \
    .gitignore

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    readme.qrc \
    $${SOURCES_DIR}/translations.qrc

