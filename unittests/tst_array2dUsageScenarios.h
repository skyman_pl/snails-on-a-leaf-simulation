#ifndef TST_array2dUsageScenarios_H
#define TST_array2dUsageScenarios_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <array2d.h>

using namespace testing;

TEST(array2dUsageScenarios, TemplateCreatesExactlyArray3x7)
{
  Utility::Array2d<int> array(4u, 8u);

  for(auto r = 0; r < 30; r+=10)
  {
    auto& newRow = array.prepareRowBack();
    for(auto c = 0; c < 7; ++c)
    {
      newRow.AddCellBack(r+c); //11, 12, ..., 21, ...
    }
  }

  EXPECT_EQ(array.getIntendedRowsCount(), 4u);
  EXPECT_EQ(array.getIntendedColsCount(), 8u);

  EXPECT_EQ(array.getRowsCount(), 3u);
  EXPECT_EQ(array.getColsCount(), 7u);

  EXPECT_EQ(array.At(0,0), 0);
  EXPECT_EQ(array.At(0,3), 3);
  EXPECT_EQ(array.At(0,6), 6);

  EXPECT_EQ(array.At(1,0), 10);
  EXPECT_EQ(array.At(1,4), 14);
  EXPECT_EQ(array.At(1,6), 16);

  EXPECT_EQ(array.At(2,0), 20);
  EXPECT_EQ(array.At(2,5), 25);
  EXPECT_EQ(array.At(2,6), 26);
}

TEST(array2dUsageScenarios, ReadingDataFromArray3x7)
{
  Utility::Array2d<int> array(4u, 8u);

  for(auto r = 0; r < 30; r+=10)
  {
    auto& newRow = array.prepareRowBack();
    for(auto c = 0; c < 7; ++c)
    {
      newRow.AddCellBack(r+c); //11, 12, ..., 21, ...
    }
  }

  EXPECT_EQ(array.At(1,2), 12);
}

TEST(array2dUsageScenarios, ModifyingDataInArray3x7)
{
  Utility::Array2d<int> array(4u, 8u);

  for(auto r = 0; r < 30; r+=10)
  {
    auto& newRow = array.prepareRowBack();
    for(auto c = 0; c < 7; ++c)
    {
      newRow.AddCellBack(r+c); //11, 12, ..., 21, ...
    }
  }

  EXPECT_EQ(array.At(0,1), 1);
  EXPECT_EQ(array.At(0,4), 4);
  EXPECT_EQ(array.At(1,3), 13);
  EXPECT_EQ(array.At(2,0), 20);
  EXPECT_EQ(array.At(2,6), 26);

  array.At(0,1) = 333;
  array.At(2,0) = 555;
  array.At(2,6) = 777;

  EXPECT_EQ(array.At(0,1), 333);
  EXPECT_EQ(array.At(0,4), 4);
  EXPECT_EQ(array.At(2,0), 555);
  EXPECT_EQ(array.At(1,3), 13);
  EXPECT_EQ(array.At(2,6), 777);
}

TEST(array2dUsageScenarios, ClearFunctionSuccessfulOnEmptyArray)
{
  Utility::Array2d<int> array(1u, 1u);

  array.Clear();
  array.Clear();
  array.Clear();
  array.Clear();
}

TEST(array2dUsageScenarios, CopyingWithAssignmentOperatorSuccessful)
{
  Utility::Array2d<int> array(99u, 99u);
  Utility::Array2d<int> arrayCopy(10u, 10u);
  Utility::Array2d<int> arrayEmptyCopy = array;

  auto& row1 = array.prepareRowBack();
  auto& row2 = array.prepareRowBack();
  auto& row3 = array.prepareRowBack();

  arrayCopy.prepareRowBack().AddCellBack(7734u);

  row1.AddCellBack(0);
  row1.AddCellBack(1);
  row2.AddCellBack(10);
  row2.AddCellBack(11);
  row3.AddCellBack(20);
  row3.AddCellBack(21);

  EXPECT_EQ(array.getRowsCount(), 3u);
  EXPECT_EQ(array.getIntendedColsCount(), 99u);
  EXPECT_EQ(array.At(0,0), 0);
  EXPECT_EQ(array.At(1,1), 11);

  EXPECT_EQ(arrayCopy.getRowsCount(), 1u);
  EXPECT_EQ(arrayCopy.getIntendedColsCount(), 10u);
  EXPECT_EQ(arrayCopy.At(0,0), 7734);

  arrayCopy = array;

  EXPECT_NE(&array, &arrayCopy);

  EXPECT_NE(arrayCopy.getRowsCount(), 1u);
  EXPECT_NE(arrayCopy.At(0,0), 7734);

  EXPECT_EQ(array.getRowsCount(), arrayCopy.getRowsCount());
  EXPECT_EQ(array.At(0,0), arrayCopy.At(0,0));
  EXPECT_EQ(array.At(1,1), arrayCopy.At(1,1));

  EXPECT_EQ(arrayEmptyCopy.getIntendedColsCount(), 99u);
  EXPECT_EQ(arrayEmptyCopy.getRowsCount(), 0u);
  EXPECT_EQ(arrayEmptyCopy.getIntendedColsCount(), array.getIntendedColsCount());
}
#endif // TST_array2dAtomicOperations_H
