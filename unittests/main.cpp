#include "tst_array2dRowtFunctionality.h"
#include "tst_array2dAtomicOperations.h"
#include "tst_array2dUsageScenarios.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
