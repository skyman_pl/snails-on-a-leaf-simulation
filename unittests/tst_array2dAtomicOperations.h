#ifndef TST_array2dAtomicOperations_H
#define TST_array2dAtomicOperations_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <array2d.h>

using namespace testing;

TEST(array2dAtomicOperations, EmptyConstructorCreatesObjectInStateZero)
{
  Utility::Array2d<int> array;

  EXPECT_EQ(array.getColsCount(), 0u);
  EXPECT_EQ(array.getRowsCount(), 0u);
  EXPECT_EQ(array.getIntendedColsCount(), 0u);
  EXPECT_EQ(array.getIntendedRowsCount(), 0u);
}

TEST(array2dAtomicOperations, ConstructorReservesMemoryForRowsAndColumns)
{
  Utility::Array2d<int> array(51u,16u);

  EXPECT_EQ(array.getIntendedRowsCount(), 51u);
  EXPECT_EQ(array.getIntendedColsCount(), 16u);
}

TEST(array2dAtomicOperations, prepareRowBackCreatesNewEmtpyRowAtEnd)
{
  Utility::Array2d<int> array(10u, 10u);
  auto& newRow = array.prepareRowBack();

  EXPECT_EQ(newRow.getColsCount(), 0u);
  EXPECT_EQ(array.getRowsCount(), 1u);
}

TEST(array2dAtomicOperations, prepareRowBackReturnsReferenceToNewRow)
{
  Utility::Array2d<int> array(10u, 10u);
  auto& newRow = array.prepareRowBack();

  newRow.AddCellBack(1);
  newRow.AddCellBack(2);
  newRow.AddCellBack(3);

  ASSERT_EQ(newRow.getColsCount(), 3u);

  newRow.AddCellBack(4);

  ASSERT_EQ(newRow.getColsCount(), 4u);
}

TEST(array2dAtomicOperations, AtConstReturnsCellAtRowAndColNumber)
{
  Utility::Array2d<int> array(10u, 10u);
  auto& newRow1 = array.prepareRowBack();
  auto& newRow2 = array.prepareRowBack();
  auto& newRow3 = array.prepareRowBack();

  newRow1.AddCellBack(11);
  newRow1.AddCellBack(12);
  newRow1.AddCellBack(13);

  newRow2.AddCellBack(21);
  newRow2.AddCellBack(22);
  newRow2.AddCellBack(23);

  newRow3.AddCellBack(31);
  newRow3.AddCellBack(32);
  newRow3.AddCellBack(33);

  const auto& constArray = array;

  ASSERT_EQ(constArray.At(0,0), 11);
  ASSERT_EQ(constArray.At(1,2), 23);
  ASSERT_EQ(constArray.At(2,1), 32);
}

TEST(array2dAtomicOperations, AtNonConstReturnsReferenceToCell)
{
  Utility::Array2d<int> array(10u, 10u);
  auto& newRow1 = array.prepareRowBack();
  auto& newRow2 = array.prepareRowBack();
  auto& newRow3 = array.prepareRowBack();

  newRow1.AddCellBack(11);
  newRow1.AddCellBack(12);
  newRow1.AddCellBack(13);

  newRow2.AddCellBack(21);
  newRow2.AddCellBack(22);
  newRow2.AddCellBack(23);

  newRow3.AddCellBack(31);
  newRow3.AddCellBack(32);
  newRow3.AddCellBack(33);

  EXPECT_EQ(array.At(2,1), 32);

  array.At(2,1) = 999;

  EXPECT_EQ(array.At(2,1), 999);
}

TEST(array2dAtomicOperations, getRowsCountReturnsNumberOfRowsInNonConstContext)
{
  Utility::Array2d<int> array(10u, 10u);
  ASSERT_EQ(array.getRowsCount(), 0u);

  array.prepareRowBack();
  array.prepareRowBack();
  array.prepareRowBack();
  ASSERT_EQ(array.getRowsCount(), 3u);

  array.prepareRowBack();
  ASSERT_EQ(array.getRowsCount(), 4u);
}

TEST(array2dAtomicOperations, getRowsCountReturnsNumberOfRowsInConstContext)
{
  Utility::Array2d<int> array(10u, 10u);
  const auto& constArray = array;
  ASSERT_EQ(constArray.getRowsCount(), 0u);

  array.prepareRowBack();
  ASSERT_EQ(constArray.getRowsCount(), 1u);
  array.prepareRowBack();
  array.prepareRowBack();

  array.prepareRowBack();
  ASSERT_EQ(constArray.getRowsCount(), 4u);
}

TEST(array2dAtomicOperations, getColsCountReturnsNumberOfColsInNonConstContext)
{
  Utility::Array2d<int> array(10u, 10u);
  auto& firstRow = array.prepareRowBack();

  array.prepareRowBack(); // second empty row
  EXPECT_EQ(array.getColsCount(), 0u);

  firstRow.AddCellBack(1);
  firstRow.AddCellBack(2);
  EXPECT_EQ(array.getColsCount(), 2u);

  firstRow.AddCellBack(3);
  EXPECT_EQ(array.getColsCount(), 3u);
}

TEST(array2dAtomicOperations, getColsCountReturnsNumberOfColsInConstContext)
{
  Utility::Array2d<int> array(10u, 10u);
  const auto& constArray = array;
  auto& firstRow = array.prepareRowBack();

  array.prepareRowBack(); // second empty row
  EXPECT_EQ(constArray.getColsCount(), 0u);

  firstRow.AddCellBack(1);
  firstRow.AddCellBack(2);
  EXPECT_EQ(constArray.getColsCount(), 2u);

  firstRow.AddCellBack(3);
  EXPECT_EQ(constArray.getColsCount(), 3u);
}

TEST(array2dAtomicOperations, getIntendedColsCountReturnsCapacityOfFirstRowInNonConstContext)
{
  const size_t colsCapacity = 9u;
  Utility::Array2d<int> array(4u, colsCapacity);
  auto& firstRow = array.prepareRowBack();

  array.prepareRowBack().SetIntendedSize(2u); // second empty row
  EXPECT_EQ(array.getIntendedColsCount(), colsCapacity);

  firstRow.AddCellBack(1);
  firstRow.AddCellBack(2);
  EXPECT_EQ(array.getIntendedColsCount(), colsCapacity);
}

TEST(array2dAtomicOperations, getIntendedColsCountReturnsCapacityOfFirstRowInConstContext)
{
  const size_t colsCapacity = 9u;
  Utility::Array2d<int> array(4u, colsCapacity);
  const auto& constArray = array;
  auto& firstRow = array.prepareRowBack();

  array.prepareRowBack().SetIntendedSize(2u); // second empty row
  EXPECT_EQ(constArray.getIntendedColsCount(), colsCapacity);

  firstRow.AddCellBack(1);
  firstRow.AddCellBack(2);
  EXPECT_EQ(constArray.getIntendedColsCount(), colsCapacity);
}

TEST(array2dAtomicOperations, getIntendedColsCountReturnsZeroAfterArrayWasCleared)
{
  Utility::Array2d<int> array(4u, 9u);

  EXPECT_EQ(array.getIntendedColsCount(), 9u);

  array.Clear();
  EXPECT_EQ(array.getIntendedColsCount(), 0u);
}

TEST(array2dAtomicOperations, getIntendedRowsCountReturnsCapacityOfRowArrayInNonConstContext)
{
  const size_t rowsCapacity = 5u;
  Utility::Array2d<int> array(rowsCapacity, 10u);

  EXPECT_EQ(array.getIntendedRowsCount(), rowsCapacity);

  array.prepareRowBack();
  array.prepareRowBack().AddCellBack(1);

  EXPECT_EQ(array.getIntendedRowsCount(), rowsCapacity);

}

TEST(array2dAtomicOperations, getIntendedRowsCountReturnsCapacityOfRowArrayInConstContext)
{
  const size_t rowsCapacity = 5u;
  Utility::Array2d<int> array(rowsCapacity, 10u);
  const auto& constArray = array;

  EXPECT_EQ(constArray.getIntendedRowsCount(), rowsCapacity);

  array.prepareRowBack();
  array.prepareRowBack().AddCellBack(1);

  EXPECT_EQ(constArray.getIntendedRowsCount(), rowsCapacity);

}

TEST(array2dAtomicOperations, ClearResetsArrayToStateZero)
{
  Utility::Array2d<int> array(10u, 10u);
  auto& newRow1 = array.prepareRowBack();
  auto& newRow2 = array.prepareRowBack();
  auto& newRow3 = array.prepareRowBack();

  newRow1.AddCellBack(11);
  newRow1.AddCellBack(12);
  newRow1.AddCellBack(13);

  newRow2.AddCellBack(21);
  newRow2.AddCellBack(22);
  newRow2.AddCellBack(23);

  newRow3.AddCellBack(31);
  newRow3.AddCellBack(32);
  newRow3.AddCellBack(33);

  EXPECT_GT(array.getRowsCount(), 0u);
  EXPECT_GT(array.getColsCount(), 0u);
  EXPECT_GT(array.getIntendedRowsCount(), 0u);
  EXPECT_GT(array.getIntendedColsCount(), 0u);

  array.Clear();

  EXPECT_EQ(array.getRowsCount(), 0u);
  EXPECT_EQ(array.getColsCount(), 0u);
  EXPECT_EQ(array.getIntendedRowsCount(), 0u);
  EXPECT_EQ(array.getIntendedColsCount(), 0u);
}

#endif // TST_array2dAtomicOperations_H
