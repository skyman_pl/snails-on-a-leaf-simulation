#ifndef TST_array2dRowtFunctionality_H
#define TST_array2dRowtFunctionality_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <array2d.h>

using namespace testing;

TEST(array2dRowtFunctionality, ConstructorCreatesRow)
{
  Utility::Array2d<int>::Row_t row(1);

  ASSERT_EQ(row.getColsCount(), 0u);
}

TEST(array2dRowtFunctionality, ConstructorSetsIntedendedRowSize)
{
  Utility::Array2d<int>::Row_t row(2);

  ASSERT_EQ(row.getRowCapacity(), 2u);
}

TEST(array2dRowtFunctionality, SetIntendedSizeSetsIntededRowSize)
{
  Utility::Array2d<int>::Row_t row(1);
  row.SetIntendedSize(99);

  ASSERT_EQ(row.getRowCapacity(), 99u);
}

TEST(array2dRowtFunctionality, AddCellBackAddsCellAtBackConst)
{
  Utility::Array2d<int>::Row_t row(10);
  const int item1 = 11, item2 = 21;

  row.AddCellBack(item1);
  row.AddCellBack(item2);

  ASSERT_EQ(row.At(0), item1);
  ASSERT_EQ(row.At(1), item2);
}

TEST(array2dRowtFunctionality, AddCellBackAddsCellAtBackNonConstWithRValue)
{
  Utility::Array2d<int>::Row_t row(2);

  row.AddCellBack(34);
  row.AddCellBack(42);

  ASSERT_EQ(row.At(0), 34);
  ASSERT_EQ(row.At(1), 42);
}

TEST(array2dRowtFunctionality, AddCellBackAddsCellAtBackNonConstWithLValue)
{
  Utility::Array2d<int>::Row_t row(10);
  int item1 = 31, item2 = 24;

  row.AddCellBack(std::move(item1));
  row.AddCellBack(std::move(item2));

  ASSERT_EQ(row.At(0), item1);
  ASSERT_EQ(row.At(1), item2);
}

TEST(array2dRowtFunctionality, AtReturnsNonConstReferenceToCell)
{
  Utility::Array2d<int>::Row_t row(10);
  size_t itemIndex = 1;
  const int initialValue = 2;
  row.AddCellBack(1);
  row.AddCellBack(initialValue);
  row.AddCellBack(3);

  auto& item = row.At(itemIndex);
  ASSERT_EQ(item, 2) << "row.row.AddCellBack() failed";

  item = 99;

  ASSERT_EQ(row.At(itemIndex), 99) << "row.At() does not return a reference";
}

TEST(array2dRowtFunctionality, AtReturnsConstReferenceToCellInConstContext)
{
  Utility::Array2d<int>::Row_t row(10);
  const auto& constRow = row;
  const size_t itemIndex = 1;
  const int itemValue = 2;
  row.AddCellBack(1);
  row.AddCellBack(itemValue);
  row.AddCellBack(3);

  ASSERT_EQ(constRow.At(itemIndex), itemValue);
}

TEST(array2dRowtFunctionality, getColsCountReturnsColsCount)
{
  std::array<const size_t, 4> intendedColsArray = {0, 1, 123, 999};

  for(size_t i = 0; i < intendedColsArray.size(); ++i)
  {
    Utility::Array2d<int>::Row_t row(intendedColsArray[i]);

    EXPECT_EQ(row.getColsCount(), 0u);
  }
}

TEST(array2dRowtFunctionality, ClearRemovesCellsFromVector)
{
  Utility::Array2d<int>::Row_t row(10);
  row.AddCellBack(1);
  row.AddCellBack(2);
  row.AddCellBack(3);

  EXPECT_GT(row.getColsCount(), 0u);

  row.Clear();

  ASSERT_EQ(row.getColsCount(), 0u);
}

TEST(array2dRowtFunctionality, ClearResetsVectorToZeroState)
{
  Utility::Array2d<int>::Row_t row(10);
  row.AddCellBack(1);
  row.AddCellBack(2);
  row.AddCellBack(3);

  EXPECT_GT(row.getColsCount(), 0u);

  row.Clear();

  ASSERT_EQ(row.getColsCount(), 0u);
  ASSERT_EQ(row.getRowCapacity(), 0u);
}

#endif // TST_array2dRowtFunctionality_H
